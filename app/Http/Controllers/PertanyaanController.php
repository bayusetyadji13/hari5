<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
        $items = DB::table('questions')->get();
        return view('pages.index',compact('items'));
    }

    public function create(){
        return view('pages.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required'
        ]);

        
        $query = DB::table('questions')->insert([
            'judul'=>$request["judul"],
            'isi' =>$request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil!');
    }

    public function show($id){
        $items = DB::table('questions')->where('id',$id)->first();
        return view('pages.detail', compact('items'));
    }

    public function edit($id){
        $items = DB::table('questions')->where('id', $id)->first();

        return view('pages.edit', compact('items'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required',
        ]);
        

        $query = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil Update!');
    }

    public function destroy($id){
        $query = DB::table('questions')->where('id', $id)->delete();

        return redirect('/pertanyaan')->with('success', 'Berhasil dihapus!');
    }


}
