@extends('layouts.app')

@section('content')
<div class="m-3">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
          <div class="alert alert-success">
            {{session('success')}}
          </div>
        @endif
        <a class="btn btn-info mb-1" href="/pertanyaan/create">Add Pertanyaan</a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Isi</th>
              <th style="width: 40px">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @forelse($items as $key => $item)
              <tr>
                <td> {{ $key+1 }} </td>
                <td> {{ $item -> judul }} </td>
                <td> {{ $item -> isi }} </td>
                <td style="display: flex;">
                  <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
                  <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <form action="/pertanyaan/{{$item->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="4" align="center">Tidak ada pertanyaan</td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
@endsection