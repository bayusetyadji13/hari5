@extends('layouts.app')

@section('content')
   <div class="ml-3 mt-3">
       <div class="card card-primary">
           <div class="card-header">
               <h3 class="card-title">Buat Pertanyaan</h3>
           </div>
           <form action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukkan Judul">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror      
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control" id="isi" name="isi" rows="3">{{ old('isi', '') }}</textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror      
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Buat</button>
            </div>
            </form>
       </div>
   </div>
@endsection